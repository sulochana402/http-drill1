const http = require("http");
const { v4: uuidv4 } = require("uuid");

const server = http.createServer((request, response) => {
  if (request.method === "GET" && request.url === "/html") {
    response.writeHead(200, { "Content-Type": "text/html" });
    const content = `<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
          </body>
        </html>`;
    response.end(content);
  }

  if (request.method === "GET" && request.url === "/json") {
    response.writeHead(200, { "Content-Type": "text/html" });
    const jsonContent = `{
        "slideshow": {
          "author": "Yours Truly",
          "date": "date of publication",
          "slides": [
            {
              "title": "Wake up to WonderWidgets!",
              "type": "all"
            },
            {
              "items": [
                "Why <em>WonderWidgets</em> are great",
                "Who <em>buys</em> WonderWidgets"
              ],
              "title": "Overview",
              "type": "all"
            }
          ],
          "title": "Sample Slide Show"
        }
      }`;
    response.end(jsonContent);
  }

  if (request.method === "GET" && request.url === "/uuid") {
    response.writeHead(200, { "Content-Type": "text/html" });
    const data = {
      uuid: uuidv4(),
    };
    response.end(JSON.stringify(data));
  }

  if (request.method === "GET" && request.url.startsWith("/status/")) {
    const statusCode = parseInt(request.url.split("/")[2]);
    const status = {
      status: statusCode,
      message: http.STATUS_CODES[statusCode],
    };
    response.writeHead(200, { "Content-Type": "text/html" });
    response.end(JSON.stringify(status));
  }

  if (request.method === "GET" && request.url.startsWith("/delay/")) {
    const delayTime = parseInt(request.url.split("/")[2]);
    if (!isNaN(delayTime) && delayTime >= 0) {
      setTimeout(() => {
        response.writeHead(200, { "Content-Type": "text/html" });
        response.end("Success response");
      }, delayTime * 1000);
    } else {
      response.writeHead(404, { "Content-Type": "text/html" });
      response.end("404 not found");
    }
  }
});
server.listen(8080, () => {
  console.log("Server is listening on port 8080");
});
